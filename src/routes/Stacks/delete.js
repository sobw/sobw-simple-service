import DataClient from '../../data/dataclient';

const deleteIt = [
  {
    method: 'DELETE',
    path: '/v1/stack/{id}',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let id = request.params.id;
      // delete all albums from this stack
      await db('StackAlbums').where({stackId: id}).del();
      // and delete the stack
      await db('Stacks').where({id}).del();
      return h.response({message: 'stack deleted'}).code(200);
    }
  },
];

export default deleteIt;