import DataClient from '../../data/dataclient';

const create = [
  {
    method: 'POST',
    path: '/v1/stacks',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let {name, ownerId, coverImgUrl} = request.payload;
      
      let record = {ownerId, name};
      if(coverImgUrl) { 
        record.coverImgUrl = coverImgUrl;
      }
      else {
        coverImgUrl = '';
      }

      let [id] = await db('Stacks').insert(record);
      let [user] = await db('Users').select('username').where({id: ownerId}).limit(1);
      let stack = {id, name, ownerId, ownerName: user.username, coverImgUrl};

      return h.response(stack).code(200);
    }
  },
];

export default create;