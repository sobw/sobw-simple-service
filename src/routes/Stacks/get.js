import DataClient from '../../data/dataclient';

const get = [
  {
    method: 'GET',
    path: '/v1/stacks',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let stacks = await db('Stacks')
        .select(DataClient.stacksFields)
        .leftOuterJoin('Users', 'Stacks.ownerId', 'Users.id');

      await Promise.all(stacks.map(async (s) => {
        s.itemCount = (await db('StackAlbums').count('albumId').where({stackId: s.id}).first())['count(`albumId`)'];
      }));

      return h.response(stacks).code(200);
    }
  },
];

export default get;