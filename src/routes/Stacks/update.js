import DataClient from '../../data/dataclient';

const update = [
  {
    method: 'PUT',
    path: '/v1/stack/{id}',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let id = request.params.id;
      let {name, ownerId, coverImgUrl} = request.payload;

      let record = {name, ownerId};
      if(coverImgUrl) { record.coverImgUrl = coverImgUrl; }
      // not sure if foreign key constraints don't work on sqllite or what but,
      // doing this check here to make sure the new owner exists before updating
      // since the constraint on the table doesn't seem to be working
      let user = await db('Users').select('username').where({id: ownerId}).first();

      if(!user) {
        return h.response({message: 'invalid owner id!'}).code(400).takeover();
      }

      await db('Stacks').where({id}).update(record).returning(['id', 'name', 'ownerId', 'coverImgUrl']);
      let stack = {id, name, ownerId, ownerName: user.username, coverImgUrl};

      return h.response(stack).code(200);
    }
  },
];

export default update;