import create from './create';
import deleteIt from './delete';
import get from './get';
import getById from './getById';
import update from './update';

const routes = [
  create,
  deleteIt,
  get,
  getById,
  update,
];

export default routes;