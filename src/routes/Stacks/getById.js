import DataClient from '../../data/dataclient';
import getStackItems from '../../data/getStackItems';

const getById = [
  {
    method: 'GET',
    path: '/v1/stack/{id}',
    handler: async (request, h) => {
      let id = request.params.id;
      let db = await DataClient.create();
      
      let [stack] = await db('Stacks')
        .select(DataClient.stacksFields)
        .where({'Stacks.id': id})
        .leftOuterJoin('Users', 'Stacks.ownerId', 'Users.id');

      stack.items = await getStackItems(id);


      return h.response(stack).code(200);
    }
  },
];

export default getById;