import DataClient from '../../data/dataclient';
import { downloadAlbumCover } from '../../data/downloadAlbumCover';

const create = [
  {
    method: 'POST',
    path: '/v1/albums',
    handler: async (request, h) => {
      let db = await DataClient.create();
      if(!request.payload || !request.payload.title || !request.payload.artistId) {
        return h.response({message: 'you must provide both a title and an artist'})
        .code(400)
        .takeover();
      }
      let {title, artistId, coverImgUrl, year} = request.payload;
      let insert = {title, year};
      if(coverImgUrl) {
        insert.coverImgUrl = coverImgUrl;
      }

      let [id] = await db('Albums').insert(insert);
      let artist = await db('Artists').select('id', 'name').where({id: artistId}).first();
      await db('AlbumArtists').insert({albumId: id, artistId, isPrimary: true});

      let album = {
        id,
        title,
        year,
        coverImgUrl,
        artists: [
          artist
        ]
      };
      downloadAlbumCover(id, coverImgUrl);
      return h.response(album).code(200);
    }
  },
];

export default create;