import DataClient from '../../data/dataclient';
import getAlbumArtists from '../../data/getAlbumArtists';

const getByArtist = [
  {
    method: 'GET',
    path: '/v1/artist/{id}/albums',
    handler: async (request, h) => {
      let id = request.params.id;
      let db = await DataClient.create();
      
      let albumIdGet = await db('AlbumArtists').select('albumId').where({artistId: id});
      let albumIds = albumIdGet.map(al => al.albumId);
      let albums = [];
      await Promise.all(albumIds.map(async (aid) => {
        let album = await db('Albums').select(DataClient.albumFields).where({id: aid}).first();
        album.artists = await getAlbumArtists(aid);
        albums.push(album);
      }));
      // since the select is wierd, can't use orderBy, so sort by year here"
      albums.sort((a1, a2) => {
        return a1.year - a2.year
      });

      return h.response(albums).code(200);
    }
  },
];

export default getByArtist;