import DataClient from '../../data/dataclient';
import getAlbumArtists from '../../data/getAlbumArtists';

const getOne = [
  {
    method: 'GET',
    path: '/v1/album/{id}',
    handler: async (request, h) => {
      let id = request.params.id;
      let db = await DataClient.create();
      
      let album = await db('Albums')
        .where({id})
        .select(DataClient.albumFields)
        .first();

      if(!album) {
        return h.response({message: 'album not found!'}).code(404).takeover();
      }

      album.artists = await getAlbumArtists(id);

      return h.response(album).code(200);
    }
  },
];

export default getOne;