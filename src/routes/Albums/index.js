import albumcover from './albumcover';
import create from './create';
import deleteIt from './delete';
import getAll from './getAll';
import getByArtist from './getByArtist';
import getOne from './getOne';
import update from './update';

const routes = [
  albumcover,
  create,
  deleteIt,
  getAll,
  getByArtist,
  getOne,
  update,
];

export default routes;