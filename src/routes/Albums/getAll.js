import DataClient from '../../data/dataclient';
import getAlbumArtists from '../../data/getAlbumArtists';
// import { albumFields, artistFields } from '../../data/defaultFields';

const getAll = [
  {
    method: 'GET',
    path: '/v1/albums',
    handler: async (request, h) => {
      let db = await DataClient.create();

      let albumsCore = await db('Albums')
        .select(DataClient.albumFields)
        .orderBy('Albums.year', 'asc');

      let albums = await Promise.all(albumsCore.map(async (album) => {
        album.artists = await getAlbumArtists(album.id);
        return album;
      }));

      return h.response(albums).code(200);
    }
  },
];

export default getAll;