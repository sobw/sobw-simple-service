import DataClient from '../../data/dataclient';
import { downloadAlbumCover } from '../../data/downloadAlbumCover';
import getAlbumArtists from '../../data/getAlbumArtists';

const update = [
  {
    method: 'PUT',
    path: '/v1/album/{id}',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let id = request.params.id;
      
      let {title, coverImgUrl, year} = request.payload;
      
      let record = {};
      if(title) { record.title = title; }
      if(coverImgUrl) { record.coverImgUrl = coverImgUrl; }
      if(year) { record.year = year; }

      
      let update = await db('Albums').where({id}).update(record).returning(['id', 'title', 'coverImgUrl']);
      if(update === 0) {
        // we don't have that album
        return h.response({message: 'album not found!'}).code(404).takeover();
      }
    
      const album = await db('Albums').select(DataClient.albumFields).where({id}).first();
      let artists = await getAlbumArtists(id);

      album.artists = artists;
      if(coverImgUrl) {
        await downloadAlbumCover(id, coverImgUrl);
      }

      return h.response(album).code(200);
    }
  },
];

export default update;