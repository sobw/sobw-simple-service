import DataClient from '../../data/dataclient';

const deleteIt = [
  {
    method: 'DELETE',
    path: '/v1/album/{id}',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let id = request.params.id;
      // delete this album from any stacks it appears in
      await db('StackAlbums').where({albumId: id}).del();
      // and delete the album
      await db('Albums').where({id}).del();
      return h.response({message: 'album deleted'}).code(200);
    }
  },
];

export default deleteIt;