import DataClient from "../../data/dataclient";
import Path from 'path';

const albumcover = [
  {
    method: 'GET',
    path: '/v1/albumcover/{id}',
    handler: async function (request, h) {
      let id = request.params.id;
      const db = await DataClient.create();
      const album = await db('Albums').where({id}).select('coverImgUrl').first();
      if(!album || !album.coverImgUrl) {
        return h.response().code(204).takeover();
      }
      return h.file(album.coverImgUrl);
    }
  }
];

export default albumcover;