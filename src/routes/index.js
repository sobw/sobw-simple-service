import StacksRoutes from './Stacks/index';
import AlbumRoutes from './Albums/index';
import ArtistRoutes from './Artists/index';

const aliveOrNah = {
  method: 'GET',
  path: '/',
  options: { cors: true, auth: false },
  handler: (request, h) => {
      return h.response({message: 'still alive'}).type('text/json').code(200);
  }
}

const routes = [aliveOrNah]
  .concat(StacksRoutes)
  .concat(AlbumRoutes)
  .concat(ArtistRoutes)

export { routes }