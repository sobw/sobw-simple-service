import DataClient from '../../data/dataclient';

const getAll = [
  {
    method: 'GET',
    path: '/v1/artists',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let artists = await db('Artists').select(DataClient.artistFields).whereNot({id: 1});
      
      return h.response(artists).code(200);
    }
  },
];

export default getAll;