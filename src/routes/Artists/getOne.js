import DataClient from '../../data/dataclient';

const getOne = [
  {
    method: 'GET',
    path: '/v1/artist/{id}',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let id = request.params.id;
      
      let artist = await db('Artists').select(DataClient.artistFields).where({id}).first();
      if(!artist) {
        return h.response({message: 'artist not found!'}).code(404).takeover();
      }
      
      return h.response(artist).code(200);
    }
  },
];

export default getOne;