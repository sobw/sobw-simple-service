import DataClient from '../../data/dataclient';

const create = [
  {
    method: 'POST',
    path: '/v1/artists',
    handler: async (request, h) => {
      let db = await DataClient.create();
      if(!request.payload || !request.payload.name) {
        return h.response({message: 'you must provide a name'})
        .code(400)
        .takeover();
      }
      let {name, profile, coverImgUrl} = request.payload;
      let record = {name, profile};
      if(coverImgUrl) { record.coverImgUrl = coverImgUrl;}

      let [id] = await db('Artists').insert(record).returning(['id']);
      return h.response({id, name, profile, coverImgUrl}).code(200);
    }
  },
];

export default create;