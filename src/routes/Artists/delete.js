import DataClient from '../../data/dataclient';
import fixOrphanedAlbums from '../../data/fixOrphanedAlbums';

const deleteIt = [
  {
    method: 'DELETE',
    path: '/v1/artist/{id}',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let id = request.params.id;
      // delete this artist from any albums it appears on
      await db('AlbumArtists').where({artistId: id}).del();
      // fix orphaned albums:
      await fixOrphanedAlbums();
      // and delete the artist
      await db('Artists').where({id}).del();
      return h.response({message: 'artist deleted'}).code(200);
    }
  },
];

export default deleteIt;