import create from './create';
import deleteIt from './delete';
import getAll from './getAll';
import getOne from './getOne';
import update from './update';

const routes = [
  create,
  deleteIt,
  getAll,
  getOne,
  update,
];

export default routes;