import DataClient from '../../data/dataclient';

const update = [
  {
    method: 'PUT',
    path: '/v1/artist/{id}',
    handler: async (request, h) => {
      let db = await DataClient.create();
      let id = request.params.id;
      
      // can't modify the default Artist ;)
      if(id === '1') {
        return h.response({message: 'unable to update artist!'}).code(451).takeover();
      }

      if(!request.payload || !request.payload.name || request.payload.name === "") {
        return h.response({message: 'you must provide a name'}).code(400).takeover();
      }
      let {name, profile, coverImgUrl} = request.payload;
      let record = {name, profile};
      if(coverImgUrl) { record.coverImgUrl = coverImgUrl; }
      try {
        let update = await db('Artists').where({id}).update(record).returning(['id', 'name', 'profile', 'coverImgUrl']);
        if(update === 0) {
          // we don't have that album
          return h.response({message: 'artist not found!'}).code(404).takeover();
        }
      }
      catch(e) {
        return h.response({message: 'an error occured', e}).code(500).takeover();
      }

      return h.response({id, name, profile, coverImgUrl}).code(200);
    }
  },
];

export default update;