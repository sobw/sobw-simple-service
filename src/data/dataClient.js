import Knex from 'knex';

const debug = process.env.NODE_ENV === 'test'? false : true;

async function create() {
  const knex = Knex({
    client: "sqlite3",
    connection: {
      filename: `stacks.sqlite3`
    },
    debug,
    useNullAsDefault: true
  });

  // verify connection works 
  try {
    await knex.raw('SELECT 1 as connectionUp');
    return knex;
  }
  catch (error) {
    console.log(error);
    throw new Error(`Can't connect to db.  Ensure your connection params are sound.`);
  }
}

const albumFields = [
  'Albums.id',
  'Albums.title',
  'Albums.coverImgUrl',
  'Albums.year'
];
const artistFields = [
  'Artists.id',
  'Artists.name',
  'Artists.coverImgUrl',
  'Artists.profile',
];
const stacksFields = [
  'Stacks.id as id',
  'Stacks.name as name',
  'Stacks.coverImgUrl as coverImgUrl',
  'Stacks.ownerId as ownerId',
  'Users.username as ownerName',
]

export default { albumFields, artistFields, create, stacksFields };