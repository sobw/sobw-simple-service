import DataClient from "./dataclient";
import getAlbumArtists from "./getAlbumArtists";

const getStackItems = async function (stackId) {
  const db = await DataClient.create();
  let albumFields = ['id', 'title'];
  let albumIdGet = await db('StackAlbums').select('albumId').where({stackId});
  let albumIds = albumIdGet.map(aid => aid.albumId);
  let albums = await db('Albums').select(albumFields).whereIn('id', albumIds);

  await Promise.all(albums.map(async (a) => {
    a.artists = await getAlbumArtists(a.id);
  }));

  return albums;
}

export default getStackItems;