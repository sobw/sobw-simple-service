import DataClient from './dataclient';

const fixOrphanedAlbums = async function() {
  const db = await DataClient.create();
  // find any albums which no longer have an artist assigned
  let orphans = await db('Albums')
    .select('Albums.id')
    .leftJoin('AlbumArtists', 'Albums.id', 'AlbumArtists.albumId')
    .whereRaw('AlbumArtists.albumId IS NULL');

  await Promise.all(orphans.map(async (o) => {
    await db('AlbumArtists').insert({albumId: o.id, artistId: 1})
  }));
}

export default fixOrphanedAlbums;