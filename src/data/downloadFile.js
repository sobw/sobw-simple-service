import * as stream from 'stream';
import { promisify } from 'util';
import fs from 'fs';
import axios from 'axios';

const finished = promisify(stream.finished);

export async function downloadFile(fileUrl, outputLocationPath) {
  const writer = fs.createWriteStream(outputLocationPath);
  return axios({
    method: 'get',
    url: fileUrl,
    responseType: 'stream',
  }).then(async response => {
    response.data.pipe(writer);
    return finished(writer); //this is a Promise
  });
}
