import DataClient from "./dataclient";

const getAlbumArtists = async function (albumId) {
  const db = await DataClient.create();

  let artistIdGet = await db('AlbumArtists').select('artistId', 'isPrimary').where({albumId});
  let primaryId = -1;
  let artistIds = artistIdGet.map((aid) => {
    if(aid.isPrimary) {
      primaryId = aid.artistId;
    }
    return aid.artistId
  });
  let artists = await db('Artists').select(DataClient.artistFields).whereIn('id', artistIds);
  for(let artist of artists) {
    artist.isPrimary = artist.id === primaryId;
  }
  return artists;
}

export default getAlbumArtists;