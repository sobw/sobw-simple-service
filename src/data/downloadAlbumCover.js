import DataClient from './dataClient';
import { downloadFile } from './downloadFile';

// silent function to download an album cover and update the album
// fire it and forget it
const downloadAlbumCover = async function(albumId, coverUrl) {
  let db = await DataClient.create();
  let test = coverUrl.match(/(\.jpe?g|\.gif|\.bmp)$/);
  let ext = test ? test[0] : '.png';
  
  
  try {
    let album = await db('Albums').where({id: albumId}).select('id', 'title').first();
    if(!album) {
      // can't really do anything here, so...
      console.log('hard to download a cover for a non-existent album!')
      return;
    }
    let filename = `${album.id}_${album.title}${ext}`.replace(/\s/g, '_');
    let dlPath = `albumcovers/${filename}`;
    const fileTry = await downloadFile(coverUrl, dlPath);
    // we seem to have survived.  update the album
    await db('Albums').update({coverImgUrl: dlPath}).where({id: albumId});
  }
  catch(e) {
    console.log('unable to download file v:)v', e);
  }
}

export { downloadAlbumCover };