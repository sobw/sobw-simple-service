import { Server } from "@hapi/hapi";
import * as laabr from 'laabr';
import * as Blipp from 'blipp';
import inert from '@hapi/inert';

import Path from 'path';

// import Jwt from '@hapi/jwt';
// @ts-ignore
// import * as SECRETS from './secrets/config.json';
// import { AUTH_CONFIG } from './utils/Tokens';

import { routes } from './routes';

const testing = process.env.NODE_ENV === 'test';

const laabrOpts = {
  formats: { onPostStart: ':time :start :level :message' },
  tokens: { 
    start:  () => '[start]',
    payload: (p) => {
      if(p.payload && p.payload.pass) {
        return '[payload]'
      }
      return p.payload;
    },
  },
  indent: 0,
  colored: true,
};

export const init = async () => {
  // console.log(Path.join(__dirname, '../'));
  const server = new Server({
      port: 3000,
      host: 'localhost',
      routes: {
        cors: {
          origin: ['*'],
        },
        files: {
          relativeTo: Path.join(__dirname, '../'),
        }
      }
  });

  if(!testing) {
    // register plugins
    await server.register({
      plugin: laabr,
      options: laabrOpts,
    });
    await server.register({
      plugin: Blipp, 
      options: { showAuth: true }
    });
  }

  await server.register([inert]);

  // await server.register(Jwt);
  // server.auth.strategy("jwt", "jwt", AUTH_CONFIG);
  // server.auth.default('jwt');

  // add all the routes.
  routes.forEach((route) => {
    server.route(route);
  });

  server.ext('onPreResponse', (request, h) => {
    const response = request.response;
    const endpoint = request.path;
    if(endpoint.match(/albumcover/)) {
      console.log('>>>>>>>>', response.statusCode);
    }
    if (endpoint.match(/albumcover/) && response.statusCode === 404 || !response.statusCode) {
        return h.response({message: 'not ready yet'}).code(100);
    }
    return h.continue;
  }); 



  await server.start();
  server.log('info', `Stacks Simple Service server running LIKE A BAWSE on ${server.info.uri}`);
  return server;
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

// init();
