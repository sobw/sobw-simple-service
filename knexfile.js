module.exports = {
  development: {
    client: "sqlite3",
    connection: {
    filename: `stacks.sqlite3`
    },
    migrations: { tableName: 'stacks-migrations' },
    seeds: { tableName: './seeds' },
    debug: true,
    useNullAsDefault: true
  }
}
