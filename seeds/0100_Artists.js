
exports.seed = async function(knex) {
  await knex('Artists').del();
  await knex('Artists').insert([
    { id: 1, name: '- none assigned -'},
    { id: 2, name: 'KISS'},
    { id: 3, 
      name: 'Aerosmith', 
      coverImgUrl: 'https://img.discogs.com/YSDtVl-oKi27zcgmEv5VhBgU4vI=/480x266/smart/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/A-48424-1373210033-8849.jpeg.jpg',
      profile: 'Aerosmith is an American rock band, sometimes referred to as "the Bad Boys from Boston" and "America\'s Greatest Rock and Roll Band". Their style, which is rooted in blues-based hard rock, has come to also incorporate elements of pop rock, heavy metal, and rhythm and blues, and has inspired many subsequent rock artists. They were formed in Boston, Massachusetts in 1970. Guitarist Joe Perry and bassist Tom Hamilton, originally in a band together called the Jam Band, met up with vocalist Steven Tyler, drummer Joey Kramer, and guitarist Ray Tabano, and formed Aerosmith. In 1971, Tabano was replaced by Brad Whitford, and the band began developing a following in Boston.'}
  ])
};
