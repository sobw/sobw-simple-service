var GUID = require('node-uuid');
var randtoken = require('rand-token');

var d = new Date();

exports.seed = function(knex, Promise) {
  // delete sessions first, if we have any
  // return knex('Sessions').del()
    // .then(function () {
    // delete usernames
    // return knex('Usernames').del()
      // .then(function () {
      // Deletes ALL existing entries
      return knex('users').del()
        .then(function () {
          // Inserts seed entries
          return knex('users').insert([
            {
              guid: GUID.v4(),
              username: 'StacksBot0874',
              password: '$2a$10$tN.pTNFJwPY6D46nDUQY0uADacLdkOyCkOnSpYdnNfkBtagZZB.h2',
              email: 'system@stacksofblackwax.com',
              activate_token: randtoken.generate(40),
              reset_token: randtoken.generate(40),
              reset_expires_at: new Date(d.getTime() + 30 * 60000)
            },
            {
              guid: GUID.v4(),
              username: 'PurpleFerret9907',
              password: '$2a$10$meU3C3fkp5qWx90dInjTzOz9Xo9xlv.stZbkR28aNtQBJA2NmHH92',
              email: 'hab@cowtao.com',
              activated: true
            },
          ]);
        });
    //   });
    // });
};
