
exports.seed = async function(knex) {
  await knex('StackAlbums').del();
  await knex('StackAlbums').insert([
    { stackId: 1, albumId: 1 },
    { stackId: 1, albumId: 2 },
    { stackId: 2, albumId: 3 },
    { stackId: 2, albumId: 4 },
    { stackId: 2, albumId: 5 },
    { stackId: 2, albumId: 6 },
    { stackId: 1, albumId: 7 },
  ]);
};
