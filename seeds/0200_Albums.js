
exports.seed = async function(knex) {
  await knex('Albums').del();
  await knex('Albums').insert([
    { id: 1, title: 'Aerosmith', year: 1973},
    { id: 2, title: 'KISS', year: 1974},
    { id: 3, title: 'Rock N Roll Over', year: 1976},
    { id: 4, title: 'Love Gun', year: 1977},
    { id: 5, title: 'Destroyer', year: 1976},
    { id: 6, title: 'Pump!', year: 1989},
  ])
};
