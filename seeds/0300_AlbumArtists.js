
exports.seed = async function(knex) {
  await knex('AlbumArtists').del();
  await knex('AlbumArtists').insert([
    { albumId: 1, artistId: 3, isPrimary: true },
    { albumId: 2, artistId: 2, isPrimary: true },
    { albumId: 3, artistId: 2, isPrimary: true },
    { albumId: 4, artistId: 2, isPrimary: true },
    { albumId: 5, artistId: 2, isPrimary: true },
    { albumId: 6, artistId: 3, isPrimary: true },
  ])
};
