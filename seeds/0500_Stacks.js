
exports.seed = async function(knex) {
  await knex('Stacks').del();
  await knex('Stacks').insert([
    { ownerId: 2, name: 'My First Stack' },
    { ownerId: 2, name: 'My Second Stack' },
  ]);
};
