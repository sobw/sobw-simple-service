
exports.up = function(knex, Promise) {
  return knex.schema.dropTableIfExists('Users')
  .createTable('Users', (usersTable) => {
      usersTable.increments();
      usersTable.string('username', 64).notNullable().unique();
      usersTable.string('email', 256).notNullable();
      usersTable.string('password', 128).notNullable();
      usersTable.string('guid', 64).notNullable().unique();
      usersTable.timestamp('created_at').notNullable().defaultTo('2018-01-01 00:00:00');
      usersTable.boolean('needs_reset').defaultTo(false);
      usersTable.timestamp('last_login');
      usersTable.boolean('activated').defaultTo(false);
      usersTable.string('activate_token', 40);
      usersTable.string('reset_token', 40);
      usersTable.timestamp('reset_expires_at');
  })
  .dropTableIfExists('Animals')
  .createTable('Animals', function(animalsTable) {
      animalsTable.increments();
      animalsTable.string('name', 32).notNullable().unique();
      animalsTable.integer('popularity').defaultTo(0);
  })
  .dropTableIfExists('Colors')
  .createTable('Colors', function(colorsTable) {
      colorsTable.increments();
      colorsTable.string('name', 16).notNullable().unique();
      colorsTable.integer('popularity').defaultTo(0);
  });
};

exports.down = function(knex, Promise) {
return knex.schema
  .dropTableIfExists('Colors')
  .dropTableIfExists('Animals')
  .dropTableIfExists('Users');
};
