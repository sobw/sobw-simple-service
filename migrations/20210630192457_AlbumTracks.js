
exports.up = function(knex) {
  return knex.schema
    .dropTableIfExists('AlbumTracks')
    .createTable('AlbumTracks', (at) => {
      at.increments();
      at.string('name', 128).notNullable();
      at.string('length', 16);
    });
};

exports.down = function(knex) {
  return knex.schema
    .dropTableIfExists('AlbumTracks');
};
