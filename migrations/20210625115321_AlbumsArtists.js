
exports.up = function(knex) {
  return knex.schema
  .dropTableIfExists('Artists')
  .createTable('Artists', (a) => {
    a.increments();
    a.string('name', 255).notNullable().unique();
    a.string('coverImgUrl', 255).defaultTo(null);
    a.text('profile', 'mediumtext');
    a.timestamps(true, true);
  })
  .dropTableIfExists('Albums')
  .createTable('Albums', (al) => {
    al.increments();
    al.string('title', 255).notNullable();
    al.string('coverImgUrl', 255).defaultTo(null);
    al.integer('year');
  })
  .dropTableIfExists('AlbumArtists')
  .createTable('AlbumArtists', (aa) => {
    aa.integer('albumId').references('id').inTable('Albums').notNullable();
    aa.integer('artistId').references('id').inTable('Artists').notNullable();
    aa.boolean('isPrimary').defaultTo(false);
    aa.unique(['artistId', 'albumId']);
  });
};

exports.down = function(knex) {
  return knex.schema
    .dropTableIfExists('AlbumArtists')
    .dropTableIfExists('Albums')
    .dropTableIfExists('Artists');
};
