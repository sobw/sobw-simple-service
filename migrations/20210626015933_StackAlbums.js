
exports.up = function(knex) {
  return knex.schema
    .dropTableIfExists('StackAlbums')
    .createTable('StackAlbums', (sa) => {
      sa.increments();
      sa.integer('stackId').references('id').inTable('Stacks').notNullable();
      sa.integer('albumId').references('id').inTable('Albums').notNullable();
      sa.unique(['stackId', 'albumId']);
    });
};

exports.down = function(knex) {
  
};
