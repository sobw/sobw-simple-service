
exports.up = function(knex) {
  return knex.schema
    .dropTableIfExists('Stacks')
    .createTable('Stacks', (s) => {
      s.increments();
      s.integer('ownerId').references('id').inTable('Users').notNullable();
      s.string('name', 255).notNullable();
      s.string('coverImgUrl', 255).defaultTo(null);
      s.timestamps(true, true);
    })
};

exports.down = function(knex) {
  return knex.schema
    .dropTableIfExists('Stacks')
};
